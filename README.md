# MOST_WormMicroscope

## Description
A user unterface created using pyqt5 that controls a 3D printer with a microscope attached. Designed to take Z stacked images of a well plate. 

## Installation
Install the required libraries. 
```
pip -r requirements.txt
```
## Usage
1. Connect the printer and microscope via USB to you computer
2. Launch the GUI
```
python gui.py
```

3. Select the camera index for the microscope and the Serial Port for the printer.

![Select](.readme/select.png)

4.Wait for the printer to home, this is done automatically.

![Homed](.readme/homed.png)

5.Using coordinate entry move the head to the approximate location of the bottom left well. Enter coordinates and click *Move to Coords*.

![Approx](.readme/aprox.png)

6.Select *Show Calibration Guide* This will create a circle in the center of the image, its size can be changed with Circle Radius. For fine control of circle size, the mouse wheel can be used after clicking on the slider. 
Adjust the size of the circle and align the microscope with the first well. 

![Align1](.readme/align1.png)

7.Click *Cal Point 1* this sets the position of the first well. 

8.Using the camera controls, move to either the well directly above the first well, or to the well directly to the right. Align the circle with this well.

![Align2](.readme/align2.png)

9.Click *Cal Point 2* this sets the calibration for the distance between wells. Be careful in this step as any error in alignment will propogate.

10.Ensure that the values in *# Horizontal Wells*, *# Vertical Wells*, and *#Z step* are correct. *# Z step* represents the number of vertical upward steps of 0.1mm from the point set in *Cal Point 1*  

11.Click *Start* this will move the microscope through every well, taking *#Z step* images for each well. The images are save to */images/* as .png files. The naming schema is 
```
<X cell>_<Y Cell>_<Z step>.png
``` 
Note that the images will write over themselves so ensure that you move them out of the folder after the trial has been run. 
