from PyQt5 import QtGui
from PyQt5.QtWidgets import QWidget, QApplication, QLabel, QVBoxLayout, QPushButton, QGridLayout,QLineEdit, QCheckBox,QSlider,QComboBox
from PyQt5.QtGui import QPixmap, QPainter,QBrush,QPen
from PyQt5.QtCore import pyqtSignal, pyqtSlot, Qt, QThread, QObject
from PyQt5 import QtCore
import numpy as np
import sys
import cv2 as cv
import serial.tools.list_ports

import serial
import time

class printControl(QThread):
    emitPos=pyqtSignal(np.ndarray)
    SP=pyqtSignal(bool)

    def __init__(self):
        super().__init__()
        

    def run(self):
        print(self.sp)
        if self.command=="H":self.home()
        if self.command=="M":self.move()
        
        

    def move(self):        
        #time.sleep(2)
        self.ser.write(bytes('G1 X{} Y{} Z{}\r\n'.format(self.x,self.y,self.z),'ascii'))
        self.ser.write(bytes('M400\r\n','ascii'))
        self.ser.write(bytes('M114\n','ascii'))
        self.realPos()
        #time.sleep(1)

    def home(self):
        time.sleep(2)
        self.ser.write(bytes('G28\r\n','ascii'))
        self.ser.write(bytes('M400\r\n','ascii'))
        self.realPos()

    def realPos(self):
        timeStart=time.time()
        b=self.ser.readline()
        while b.decode()[0] != 'X':
            time.sleep(.05)
            b=self.ser.readline()
        print(b.decode().split(' '))
        print([i[2:-1] for i in b.decode().split(' ')[0:3]])
        self.rx,self.ry,self.rz=[float(i[2:-1]) for i in b.decode().split(' ')[0:3]]
        print("REALPOS TIME :: {}".format(time.time()-timeStart))
        self.emitPos.emit(np.array([self.rx,self.ry,self.rz]))
        self.SP.emit(self.sp)



class liveVid(QThread):
    img2pixmap=pyqtSignal(np.ndarray)

    def __init__(self):
        super().__init__()
        self.running=True

    def run(self):
        cam=cv.VideoCapture(self.camera)
        while self.running:
            r,i=cam.read()
            if r:
                self.img2pixmap.emit(i)
        print("Lost Video")
        cam.release()
    def stop(self):
        self.running=False
        self.wait()


class queuePrinter(QThread):

    def __init__(self):
        super().__init__()

    def run(self):
        print(self.arg)


class App(QWidget):
    posSend=pyqtSignal(np.ndarray)

    def __init__(self,port,camera):
        super().__init__()
        self.port=port
        self.camera=camera
        self.serial=serial.Serial(self.port,250000)
        self.printHome()
        self.setWindowTitle("Live Microscope View")
        self.il = QLabel(self)
        self.il.setBaseSize(640, 480)
        self.bt= QPushButton("Home")


        self.c1x=None
        self.c1y=None
        self.c1z=None

        self.c2x=None
        self.c2y=None
        self.c2z=None

        self.distance=None
        self.zStep=0.1

        self.calRad=1

        self.well=0

        self.queue=[]

        vbox = QVBoxLayout()
        vbox.addWidget(self.il)
        vbox.addWidget(self.bt)

        locale=QGridLayout()
        xLab=QLabel("X:")
        self.xLoc=QLineEdit("~")
        self.xLoc.setMaximumWidth(40)
        xLab.setMaximumWidth(10)
        self.xDown=QPushButton("-")
        self.xDown.setMaximumWidth(40)
        locale.addWidget(self.xDown,0,1)
        self.xUp=QPushButton("+")
        self.xUp.setMaximumWidth(40)
        locale.addWidget(self.xUp,0,3)
        locale.addWidget(xLab,0,0)
        locale.addWidget(self.xLoc,0,2)
        yLab=QLabel("Y:")
        self.yLoc=QLineEdit("~")
        self.yLoc.setMaximumWidth(40)
        self.yDown=QPushButton("-")
        self.yDown.setMaximumWidth(40)
        locale.addWidget(self.yDown,1,1)
        self.yUp=QPushButton("+")
        self.yUp.setMaximumWidth(40)
        locale.addWidget(self.yUp,1,3)
        locale.addWidget(yLab,1,0)
        locale.addWidget(self.yLoc,1,2)
        zLab=QLabel("Z:")
        self.zLoc=QLineEdit("~")
        self.zLoc.setMaximumWidth(40)
        self.zDown=QPushButton("-")
        self.zDown.setMaximumWidth(40)
        locale.addWidget(self.zDown,2,1)
        self.zUp=QPushButton("+")
        self.zUp.setMaximumWidth(40)
        locale.addWidget(self.zUp,2,3)
        locale.addWidget(zLab,2,0)
        locale.addWidget(self.zLoc,2,2)
        self.moveCoord=QPushButton("Move to Coords")
        self.moveCoord.setMaximumWidth(90)
        locale.addWidget(self.moveCoord,3,0,1,4,alignment=Qt.AlignCenter)

        
        calibration=QGridLayout()
        self.showGuide=QCheckBox()
        self.showGuide.setMaximumWidth(15)
        labGuide=QLabel("Show Calibration Guide")
        labGuide.setMaximumWidth(120)
        self.calSlider=QSlider()
        self.calSlider.setMaximum(500)
        self.calSlider.setMinimum(1)
        self.calSlider.setOrientation(Qt.Horizontal)
        self.calSlider.setMaximumWidth(270)
        self.calSlider.valueChanged.connect(self.calSize) 
        labSlider=QLabel("Circle Radius")
        labSlider.setMaximumWidth(120)  
        self.cal1=QPushButton("Cal Point 1")
        self.cal2=QPushButton("Cal Point 2")

        labelHor=QLabel("# Horizontal Wells")
        labelHor.setMaximumWidth(500)
        self.nHor=QLineEdit("12")
        self.nHor.setMaximumWidth(40)

        labelVer=QLabel("# Vertical Wells")
        labelVer.setMaximumWidth(500)
        self.nVer=QLineEdit("8")
        self.nVer.setMaximumWidth(40)

        labelUp=QLabel('# Z step')
        labelUp.setMaximumWidth(500)
        self.nZ=QLineEdit('10')
        self.nZ.setMaximumWidth(40)

        self.startRun=QPushButton("Start")
        self.startRun.setMaximumWidth(90)



        calibration.addWidget(labGuide,0,0,alignment=Qt.AlignRight)
        calibration.addWidget(self.showGuide,0,1,alignment=Qt.AlignLeft)
        calibration.addWidget(labSlider,1,0,alignment=Qt.AlignRight)
        calibration.addWidget(self.calSlider,1,1,alignment=Qt.AlignLeft)
        calibration.addWidget(self.cal1,2,0,alignment=Qt.AlignRight)
        calibration.addWidget(self.cal2,2,1,alignment=Qt.AlignLeft)
        calibration.addWidget(labelHor,3,0,alignment=Qt.AlignRight)
        calibration.addWidget(self.nHor,3,1,alignment=Qt.AlignLeft)
        calibration.addWidget(labelVer,4,0,alignment=Qt.AlignRight)
        calibration.addWidget(self.nVer,4,1,alignment=Qt.AlignLeft)
        calibration.addWidget(labelUp,5,0,alignment=Qt.AlignRight)
        calibration.addWidget(self.nZ,5,1,alignment=Qt.AlignLeft)
        calibration.addWidget(self.startRun,6,0,1,2,alignment=Qt.AlignCenter)

        gridLay=QGridLayout()
        gridLay.addLayout(vbox,0,0,1,2)
        gridLay.addLayout(locale,1,0)
        gridLay.addLayout(calibration,1,1)

        self.setLayout(gridLay)
        
        self.bt.clicked.connect(self.printHome)
        self.moveCoord.clicked.connect(self.printMove)

        self.xUp.clicked.connect(lambda: self.printMove(dx=0.1))
        self.xDown.clicked.connect(lambda: self.printMove(dx=-0.1))
        self.yUp.clicked.connect(lambda: self.printMove(dy=0.1))
        self.yDown.clicked.connect(lambda: self.printMove(dy=-0.1))
        self.zUp.clicked.connect(lambda: self.printMove(dz=0.1))
        self.zDown.clicked.connect(lambda: self.printMove(dz=-0.1))

        self.cal1.clicked.connect(self.calibrationOne)
        self.cal2.clicked.connect(self.calibrationTwo)

        self.startRun.clicked.connect(self.returnCal1)

        self.thread = liveVid()
        self.thread.camera=self.camera
        self.thread.img2pixmap.connect(self.updateImage)
#        self.bt.clicked.connect(self.thread.commsTest)
        self.thread.start()

        self.setFixedSize(640,700)

    def closeEvent(self,event):
        self.thread.stop()
        event.accept()

    def sendPos(self):
        self.posSend.emit(np.array(self.tx,self.ty,self.tz))


    def printHome(self):
        self.printControl=printControl()
        self.printControl.ser=self.serial
        self.command="H"
        self.printControl.command=self.command
        self.printControl.emitPos.connect(self.updatePos)
        self.printControl.SP.connect(self.isMoving)
        self.printControl.sp=False
        self.printControl.start()

    def printMove(self,dx=0,dy=0,dz=0,sp=False):
        if self.printControl.isRunning():
            print("Adding to Queue")
            self.queue.append([dx,dy,dz,self.xLoc.text(),self.yLoc.text(),self.zLoc.text(),sp])
        else:
            print("SP:: {}".format(sp))
            self.printControl=printControl()
            self.printControl.ser=self.serial
            self.printControl.x=float(self.xLoc.text())+dx
            self.printControl.y=float(self.yLoc.text())+dy
            self.printControl.z=float(self.zLoc.text())+dz
            self.command="M"
            self.printControl.command=self.command
            self.printControl.sp=sp
            self.printControl.emitPos.connect(self.updatePos)
            self.printControl.SP.connect(self.isMoving)
            self.printControl.start()

    def calibrationOne(self):
        self.c1x=float(self.xLoc.text())
        self.c1y=float(self.yLoc.text())
        self.c1z=float(self.zLoc.text())
        print(self.c1x, self.c1y, self.c1z)

    def calibrationTwo(self):
        self.c2x=float(self.xLoc.text())
        self.c2y=float(self.yLoc.text())
        self.c2z=float(self.zLoc.text())
        print(self.c2x, self.c2y, self.c2z)
        c1=np.array((self.c1x,self.c1y,self.c1z))
        c2=np.array((self.c2x,self.c2y,self.c2z))
        self.distance=np.linalg.norm(abs(c1-c2))
        print("DISTANCE :: {}".format(self.distance))

    def returnCal1(self):
        self.xLoc.setText(str(self.c1x))
        self.yLoc.setText(str(self.c1y))
        self.zLoc.setText(str(self.c1z))
        self.printMove()
        self.planeHop()

    def planeHop(self):
        cells=np.zeros((int(self.nVer.text()),int(self.nHor.text()),3))
        for v in range(0,int(self.nVer.text())):
            for h in range(0,int(self.nHor.text())):
                for z in range(0,int(self.nZ.text())):
                    cells[v,h]=[(self.c1x+h*self.distance),(self.c1y+v*self.distance),(str(self.c1z))]
                    self.xLoc.setText(str(self.c1x+h*self.distance))
                    self.yLoc.setText(str(self.c1y+v*self.distance))
                    self.zLoc.setText(str(self.c1z+z*self.zStep))
                    self.printMove(0,0,0,True)
        #print(cells)



    @pyqtSlot(np.ndarray)
    def updateImage(self,i):
        self.image=i
        qI=self.conv(i)
        self.il.setPixmap(qI)
    def conv(self,i):
        i=cv.cvtColor(i,cv.COLOR_BGR2RGB)
        y,x,d=i.shape
        qI=QPixmap.fromImage(QtGui.QImage(i.data,x,y,d*x,QtGui.QImage.Format_RGB888))
        if self.showGuide.isChecked():
            self.paintCircle(qI)
        return qI

    def paintCircle(self,img):
        paint=QPainter(img)
        paint.setPen(QPen(Qt.red,2,Qt.DotLine))
        paint.drawEllipse(320-round(self.calRad/2),240-round(self.calRad/2),self.calRad,round(self.calRad))
    def calSize(self,args):
        self.calRad=args

    @pyqtSlot(np.ndarray)
    def updatePos(self,pos):
        self.xLoc.setText(str(pos[0]))
        self.yLoc.setText(str(pos[1]))
        self.zLoc.setText(str(pos[2]))
    @pyqtSlot(bool)
    def isMoving(self,b):
        print("Finished Moving")
        if b:
            dx=self.c1x-float(self.xLoc.text())
            dy=self.c1y-float(self.yLoc.text())
            dz=self.c1z-float(self.zLoc.text())
            wx=abs(round(dx/self.distance))
            wy=abs(round(dy/self.distance))
            wz=abs(round(dz/self.zStep))

            cv.imwrite('./images/'+str(wx)+'_'+str(wy)+'_'+str(wz)+'.png',self.image)
            self.well+=1
        if len(self.queue)>0:
            q=self.queue.pop(0)
            self.xLoc.setText(q[3])
            self.yLoc.setText(q[4])
            self.zLoc.setText(q[5])
            self.printMove(q[0],q[1],q[2],q[6])

class Sel(QWidget):
    def __init__(self):
        super().__init__()
        cams=self.findCamera()
        self.ports=self.findPorts()


        self.setWindowTitle("Select Devices")
        layout=QGridLayout()
        labelCam=QLabel("Select Camera")
        self.select=QComboBox()
        self.select.addItems(cams)
        self.select.activated.connect(self.getCamera)
        labelPort=QLabel("Select Port for Printer")
        self.selectPort=QComboBox()
        self.selectPort.addItems(self.ports)
        self.selectPort.activated.connect(self.getPort)
        self.selectPort.setEnabled(False)
        self.closeBut=QPushButton("Next")
        self.closeBut.clicked.connect(self.close)
        self.closeBut.setEnabled(False)
        layout.addWidget(labelCam)
        layout.addWidget(self.select)
        layout.addWidget(labelPort)
        layout.addWidget(self.selectPort)
        layout.addWidget(self.closeBut)



        self.setLayout(layout)

    def getCamera(self,arg):
        self.cameraI=arg
        self.selectPort.setEnabled(True)
    def getPort(self,arg):
        self.portI=self.ports[arg]
        self.closeBut.setEnabled(True)

    def findCamera(self):
        cams=[]
        for i in range(0,10):
            cam=cv.VideoCapture(i)
            r,img=cam.read()
            if r:
                cams.append(str(i))
        return cams

    def findPorts(self):
        return [x[0] for x in serial.tools.list_ports.comports(include_links=True)]

    def closeEvent(self,event):
        a=App(self.portI,self.cameraI)
        a.show()
        event.accept()



if __name__=="__main__":
    app=QApplication([])
    b=Sel()
    b.show()
    #a=App()
    #a.show()
    sys.exit(app.exec_())
